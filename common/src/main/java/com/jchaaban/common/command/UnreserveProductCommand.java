package com.jchaaban.common.command;

import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class UnreserveProductCommand {

    @TargetAggregateIdentifier
    private final String productId;
    private final String orderId;
    private final int quantity;
}
