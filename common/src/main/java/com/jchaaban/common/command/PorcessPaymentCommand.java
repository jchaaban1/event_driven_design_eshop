package com.jchaaban.common.command;

import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.math.BigDecimal;

@Data
public class PorcessPaymentCommand {

    @TargetAggregateIdentifier
    private final String paymentDetailsId;
    private final String orderId;
    private final BigDecimal amountToSubtract;
}
