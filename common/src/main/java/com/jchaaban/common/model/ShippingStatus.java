package com.jchaaban.common.model;

public enum ShippingStatus {
    DELIVERED, IN_TRANSIT
}
