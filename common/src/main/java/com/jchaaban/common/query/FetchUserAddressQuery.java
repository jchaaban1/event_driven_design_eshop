package com.jchaaban.common.query;

import lombok.Data;

@Data
public class FetchUserAddressQuery {
    private final String userId;
}
