package com.jchaaban.common.query;

import lombok.Data;

import java.util.List;

@Data
public class FetchProductsPriceQuery {

    private final List<String> reservedProducts;
}
