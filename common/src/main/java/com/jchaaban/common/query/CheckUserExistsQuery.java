package com.jchaaban.common.query;

import lombok.Data;

@Data
public class CheckUserExistsQuery {

    private final String userId;
}
