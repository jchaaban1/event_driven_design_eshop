package com.jchaaban.common.event;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductUnreservedEvent {

    private final String orderId;
    private final String productId;
    private final int quantity;
    private final BigDecimal productPrice;
}
