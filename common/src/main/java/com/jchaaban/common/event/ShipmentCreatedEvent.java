package com.jchaaban.common.event;

import com.jchaaban.common.model.Address;
import com.jchaaban.common.model.ShippingStatus;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ShipmentCreatedEvent {
    private final String shipmentId;
    private final String orderId;
    private final Address address;
    private final ShippingStatus shippingStatus;
    private final String trackingNumber;
    private final BigDecimal shippingCost;
    private final BigDecimal totalPrice;
}
