package com.jchaaban.common.event;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PaymentProcessedEvent {
    private final String paymentDetailsId;
    private final String orderId;
    private final BigDecimal orderTotalPrice;
}
