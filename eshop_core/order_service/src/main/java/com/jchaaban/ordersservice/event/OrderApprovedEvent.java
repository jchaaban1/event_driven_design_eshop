package com.jchaaban.ordersservice.event;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderApprovedEvent {
    private final String orderId;
    private final String userId;
    private final BigDecimal orderTotalPrice;
}
