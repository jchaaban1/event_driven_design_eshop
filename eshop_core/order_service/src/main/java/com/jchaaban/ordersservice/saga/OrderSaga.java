package com.jchaaban.ordersservice.saga;

import com.jchaaban.common.event.PaymentProcessedEvent;
import com.jchaaban.common.event.ProductReservedEvent;
import com.jchaaban.common.event.ShipmentCreatedEvent;
import com.jchaaban.ordersservice.event.OrderApprovedEvent;
import com.jchaaban.ordersservice.event.OrderCreatedEvent;
import com.jchaaban.ordersservice.saga.service.OrderProcessingService;
import com.jchaaban.ordersservice.saga.service.OrderSummaryNotifier;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.logging.Logger;

@Saga
@NoArgsConstructor
public class OrderSaga {
    private static final Logger logger = Logger.getLogger(OrderSaga.class.getName());

    @Autowired
    private transient OrderProcessingService orderProcessingService;

    @Autowired
    private transient OrderSummaryNotifier orderSummaryEmitter;

    @StartSaga
    @SagaEventHandler(associationProperty = "orderId")
    public void handle(OrderCreatedEvent event) {
        logger.info("Handling OrderCreatedEvent in Saga for orderId: " + event.getOrderId());
        orderProcessingService.processOrderCreation(event);
    }

    @SagaEventHandler(associationProperty = "orderId")
    public void handle(ProductReservedEvent productReservedEvent) {
        logger.info("Handling ProductReservedEvent in Saga");
        orderProcessingService.processProductReservation(productReservedEvent);
    }

    @SagaEventHandler(associationProperty = "orderId")
    public void handle(PaymentProcessedEvent paymentProcessedEvent) {
        logger.info("Handling PaymentProcessedEvent in Saga");
        orderProcessingService.approveOrder();
    }

    @SagaEventHandler(associationProperty = "orderId")
    public void handle(OrderApprovedEvent orderApprovedEvent) {
        logger.info("Handling OrderApprovedEvent in Saga");
        orderProcessingService.processOrderApproval();
    }

    @EndSaga
    @SagaEventHandler(associationProperty = "orderId")
    public void handle(ShipmentCreatedEvent shipmentCreatedEvent) {
        logger.info("Handling ShipmentCreatedEvent in Saga");
        orderSummaryEmitter.emitOrderSummary(
                shipmentCreatedEvent.getOrderId(),
                String.format(
                        "Order processed successfully. Total cost: %s$ including shipping cost",
                        shipmentCreatedEvent.getTotalPrice())
        );
    }
}
