package com.jchaaban.ordersservice.repository;

import com.jchaaban.common.model.OrderStatus;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class OrderEntity {

    @Id
    @Column(unique = true)
    public String orderId;

    private String userId;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "order_product_ids", joinColumns = @JoinColumn(name = "order_id"))
    @Column(name = "product_ids")
    private List<String> productIds;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    private BigDecimal totalPrice;

    public OrderEntity(String userId,
                       String orderId,
                       List<String> productIds,
                       OrderStatus orderStatus,
                       BigDecimal totalPrice) {
        this.userId = userId;
        this.orderId = orderId;
        this.productIds = productIds;
        this.orderStatus = orderStatus;
        this.totalPrice = totalPrice;
    }
}
