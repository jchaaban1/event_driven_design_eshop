package com.jchaaban.ordersservice.saga.context;

import com.jchaaban.common.event.ProductReservedEvent;
import com.jchaaban.ordersservice.event.OrderCreatedEvent;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class OrderContext {

    private static final int SHIPPING_COST = 20;

    private final String orderId;
    private final String userId;
    private final Map<String, Integer> productsToReserve;
    private final Map<String, Integer> reservedProducts;
    private final int productsToReserveCount;
    private BigDecimal totalPrice;

    public OrderContext(OrderCreatedEvent event) {
        this.orderId = event.getOrderId();
        this.userId = event.getUserId();
        this.totalPrice = BigDecimal.ZERO;
        this.reservedProducts = new HashMap<>();
        this.productsToReserve = countProductOccurrences(event.getProductIds());
        this.productsToReserveCount = productsToReserve.size();
    }

    public void updateTotalPrice(ProductReservedEvent productReservedEvent) {
        BigDecimal pricePerUnit = productReservedEvent.getPrice();
        int quantity = productReservedEvent.getQuantity();
        BigDecimal totalPriceForProduct = pricePerUnit.multiply(BigDecimal.valueOf(quantity));
        totalPrice = totalPrice.add(totalPriceForProduct);
    }

    public void addReservedProduct(ProductReservedEvent productReservedEvent) {
        reservedProducts.put(productReservedEvent.getProductId(), productReservedEvent.getQuantity());
    }

    public boolean allProductsReserved() {
        return productsToReserveCount == reservedProducts.size();
    }

    public BigDecimal getTotalPriceWithShipping() {
        return totalPrice.add(BigDecimal.valueOf(SHIPPING_COST));
    }

    private Map<String, Integer> countProductOccurrences(List<String> productIds) {
        return productIds
                .stream()
                .collect(Collectors.groupingBy(productId -> productId, Collectors.summingInt(productId -> 1)));
    }
}
