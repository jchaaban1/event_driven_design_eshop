package com.jchaaban.ordersservice.rest.exception;


public class OrderNotFoundException extends RuntimeException {

    public OrderNotFoundException(String orderId) {
        super("The order with the ID: " + orderId + " was not found");
    }
}
