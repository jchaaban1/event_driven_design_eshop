package com.jchaaban.ordersservice.saga.service;

import com.jchaaban.common.dto.ReadPaymentDetailsDto;
import com.jchaaban.common.model.Address;
import com.jchaaban.common.query.CheckUserExistsQuery;
import com.jchaaban.common.query.FetchUserAddressQuery;
import com.jchaaban.common.query.FetchUserPaymentDetailsQuery;
import lombok.RequiredArgsConstructor;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserQueryService {

    private final QueryGateway queryGateway;

    public Address fetchUserAddress(String userId) {
        return queryGateway.query(new FetchUserAddressQuery(userId), ResponseTypes.instanceOf(Address.class))
                .exceptionally(ex -> null)
                .join();
    }

    public boolean userExists(String userId) {
        return queryGateway.query(new CheckUserExistsQuery(userId), Boolean.class)
                .exceptionally(ex -> null)
                .join();
    }

    public ReadPaymentDetailsDto fetchUserPaymentDetails(String userId) {
        return queryGateway.query(new FetchUserPaymentDetailsQuery(userId), ReadPaymentDetailsDto.class)
                .exceptionally(ex -> null)
                .join();
    }
}
