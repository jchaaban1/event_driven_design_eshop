package com.jchaaban.ordersservice.saga.service;

import com.jchaaban.common.query.FetchOrderQuery;
import com.jchaaban.ordersservice.rest.dto.OrderSummary;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderSummaryNotifier {

    private final QueryUpdateEmitter queryUpdateEmitter;

    public void emitOrderSummary(String orderId, String message) {
        OrderSummary summary = new OrderSummary(orderId, message);
        queryUpdateEmitter.emit(FetchOrderQuery.class, query -> true, summary);
    }
}
