package com.jchaaban.ordersservice.command;

import com.jchaaban.common.model.OrderStatus;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.List;

@Data
public class UpdateOrderCommand {

    @TargetAggregateIdentifier
    private final String orderId;
    private final String userId;
    private final List<String> productId;
    private final int quantity;
    private final String addressId;
    private final OrderStatus orderStatus;
}
