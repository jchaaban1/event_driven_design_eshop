package com.jchaaban.ordersservice.saga.service;

import com.jchaaban.common.command.ReserveProductCommand;
import com.jchaaban.common.command.UnreserveProductCommand;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Logger;

@Component
@RequiredArgsConstructor
public class ProductReservationService {

    private static final Logger logger = Logger.getLogger(ProductReservationService.class.getName());
    private static final String PRODUCT_RESERVATION_FAILED = "Reservation failed for Product having ID: %s";

    private final CommandGateway commandGateway;

    public void reserveProducts(
            Map<String, Integer> productsToReserve, String orderId,
            Consumer<String> rejectOrderCallback
    ) {
        productsToReserve.forEach((productId, quantity) -> commandGateway.send(new ReserveProductCommand(productId, orderId, quantity), (commandMessage, commandResultMessage) -> {
            if (commandResultMessage.isExceptional()) {
                rejectOrderCallback.accept(String.format(PRODUCT_RESERVATION_FAILED, productId));
            }
        }));
    }

    public void rollbackProductsReservation(Map<String, Integer> reservedProducts, String orderId) {
        logger.info("Rolling back products reservations");
        reservedProducts.forEach((productId, quantity) -> commandGateway.send(new UnreserveProductCommand(productId, orderId, quantity)));
    }
}
