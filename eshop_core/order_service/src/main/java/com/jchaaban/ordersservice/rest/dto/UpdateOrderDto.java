package com.jchaaban.ordersservice.rest.dto;

import lombok.Data;

import java.util.List;

@Data
public class UpdateOrderDto {
    private final String userId;
    private final List<String> productId;
    private final int quantity;
    private final String addressId;
}
