package com.jchaaban.ordersservice.saga.service;

import com.jchaaban.common.command.CreateShipmentCommand;
import com.jchaaban.common.model.Address;
import com.jchaaban.common.model.ShippingStatus;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ShipmentService {

    private final CommandGateway commandGateway;
    private final UserQueryService userQueryService;

    public void createShipment(String orderId, String userId, BigDecimal totalPrice, BigDecimal shippingCost) {
        Address userAddress = userQueryService.fetchUserAddress(userId);

        CreateShipmentCommand createShipmentCommand = new CreateShipmentCommand(
                UUID.randomUUID().toString(),
                orderId,
                userAddress,
                ShippingStatus.DELIVERED,
                UUID.randomUUID().toString(),
                shippingCost,
                totalPrice.add(shippingCost)
        );

        commandGateway.sendAndWait(createShipmentCommand);
    }
}
