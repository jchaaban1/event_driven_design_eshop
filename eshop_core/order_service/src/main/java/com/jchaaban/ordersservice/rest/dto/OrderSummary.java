package com.jchaaban.ordersservice.rest.dto;

import lombok.Data;
@Data
public class OrderSummary {
    public final String orderId;
    private final String message;
}
