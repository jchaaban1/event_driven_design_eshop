package com.jchaaban.ordersservice.event;

import com.jchaaban.common.model.OrderStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderCreatedEvent {
    private final String orderId;
    private final String userId;
    private final List<String> productIds;
    private final OrderStatus orderStatus;
    private final BigDecimal orderTotalPrice;
}
