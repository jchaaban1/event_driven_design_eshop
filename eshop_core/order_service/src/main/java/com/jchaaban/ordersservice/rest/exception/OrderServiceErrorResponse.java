package com.jchaaban.ordersservice.rest.exception;

import lombok.Data;

@Data
public class OrderServiceErrorResponse {
    private final String errorMessage;
}
