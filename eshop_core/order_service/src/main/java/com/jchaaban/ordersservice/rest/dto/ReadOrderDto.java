package com.jchaaban.ordersservice.rest.dto;

import com.jchaaban.common.model.OrderStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ReadOrderDto {
    public final String orderId;
    private final String userId;
    private final List<String> productId;
    private final OrderStatus orderStatus;
    private final BigDecimal orderTotalPrice;
}
