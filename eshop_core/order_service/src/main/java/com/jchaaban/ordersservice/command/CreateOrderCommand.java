package com.jchaaban.ordersservice.command;

import com.jchaaban.common.model.OrderStatus;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.List;

@Data
public class CreateOrderCommand {
    @TargetAggregateIdentifier
    private final String orderId;
    private final String userId;
    private final List<String> productIds;
    private final OrderStatus orderStatus;
}
