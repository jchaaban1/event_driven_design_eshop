package com.jchaaban.ordersservice.rest.dto;

import lombok.Data;

import java.util.List;

@Data
public class CreateOrderDto {
    private final String userId;
    private final List<String> productIds;
}
