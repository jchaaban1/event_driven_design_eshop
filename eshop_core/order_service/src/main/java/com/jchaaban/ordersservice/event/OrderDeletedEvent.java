package com.jchaaban.ordersservice.event;

import lombok.Data;

@Data
public class OrderDeletedEvent {
    private final String orderId;
}