package com.jchaaban.ordersservice.aggregate;

import com.jchaaban.common.model.OrderStatus;
import com.jchaaban.ordersservice.command.*;
import com.jchaaban.ordersservice.event.*;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Logger;

@Aggregate
@NoArgsConstructor
@Component
public class OrderAggregate {
    private static final Logger logger = Logger.getLogger(OrderAggregate.class.getName());

    @AggregateIdentifier
    private String orderId;
    private String userId;
    private List<String> productId;
    private OrderStatus orderStatus;
    private BigDecimal orderTotalPrice;


    @CommandHandler
    public OrderAggregate(CreateOrderCommand createOrderCommand) {
        logger.info("We are in on " + createOrderCommand.getClass().getName());
        OrderCreatedEvent orderCreatedEvent = new OrderCreatedEvent(
                createOrderCommand.getOrderId(),
                createOrderCommand.getUserId(),
                createOrderCommand.getProductIds(),
                createOrderCommand.getOrderStatus(),
                new BigDecimal(0));
        AggregateLifecycle.apply(orderCreatedEvent);
    }

    @EventSourcingHandler
    public void on(OrderCreatedEvent orderCreatedEvent) {
        logger.info("We are in the aggregate on EventSourcingHandler " + orderCreatedEvent.getClass().getName());
        orderId = orderCreatedEvent.getOrderId();
        userId = orderCreatedEvent.getUserId();
        productId = orderCreatedEvent.getProductIds();
        orderStatus = orderCreatedEvent.getOrderStatus();
        orderTotalPrice = orderCreatedEvent.getOrderTotalPrice();
    }

    @CommandHandler
    public void handle(DeleteOrderCommand deleteOrderCommand) {
        logger.info("We are in on " + deleteOrderCommand.getClass().getName());
        OrderDeletedEvent orderDeletedEvent = new OrderDeletedEvent(
                deleteOrderCommand.getOrderId()
        );
        AggregateLifecycle.apply(orderDeletedEvent);
    }

    @EventSourcingHandler
    public void on(OrderDeletedEvent orderDeletedEvent) {
        logger.info("We are in EventSourcingHandler on " + orderDeletedEvent.getClass().getName());
        orderId = orderDeletedEvent.getOrderId();
    }

    @CommandHandler
    public void handle(RejectOrderCommand cancelOrderCommand) {
        logger.info("We are in on " + cancelOrderCommand.getClass().getName());
        OrderRejectedEvent orderRejectedEvent = new OrderRejectedEvent(
                cancelOrderCommand.getOrderId(),
                cancelOrderCommand.getReason()
        );
        AggregateLifecycle.apply(orderRejectedEvent);
    }

    @EventSourcingHandler
    public void on(OrderRejectedEvent orderRejectedEvent) {
        logger.info("We are in EventSourcingHandler on " + orderRejectedEvent.getClass().getName());
        orderId = orderRejectedEvent.getOrderId();
    }

    @CommandHandler
    public void handle(ApproveOrderCommand approveOrderCommand) {
        logger.info("We are in the aggregate on " + approveOrderCommand.getClass().getName());
        logger.info("This is the total price " + orderTotalPrice);
        OrderApprovedEvent orderCompletedEvent = new OrderApprovedEvent(
                approveOrderCommand.getOrderId(),
                userId,
                approveOrderCommand.getOrderTotalPrice()
        );
        AggregateLifecycle.apply(orderCompletedEvent);
    }

    @EventSourcingHandler
    public void on(OrderApprovedEvent orderCompletedEvent) {
        logger.info("We are in the aggregate EventSourcingHandler on " + orderCompletedEvent.getClass().getName());
        logger.info("This is the total price " + orderTotalPrice);
        orderId = orderCompletedEvent.getOrderId();
        orderStatus = OrderStatus.APPROVED;
        orderTotalPrice = orderCompletedEvent.getOrderTotalPrice();
    }
}
