package com.jchaaban.ordersservice;

import com.jchaaban.ordersservice.interceptor.command.CommandInterceptor;
import com.jchaaban.ordersservice.config.XStreamSecurityConfig;
import com.jchaaban.ordersservice.interceptor.query.QueryInterceptor;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.queryhandling.QueryBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({ XStreamSecurityConfig.class })
public class OrdersServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersServiceApplication.class, args);
	}

	@Autowired
	public void registerInterceptors(
			ApplicationContext context,
			CommandBus commandBus,
			QueryBus queryBus
	){
		commandBus.registerDispatchInterceptor(context.getBean(CommandInterceptor.class));
		queryBus.registerHandlerInterceptor(context.getBean(QueryInterceptor.class));
	}
}
