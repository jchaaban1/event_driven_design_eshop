package com.jchaaban.ordersservice.handler.command;

import com.jchaaban.common.model.OrderStatus;
import com.jchaaban.ordersservice.event.OrderApprovedEvent;
import com.jchaaban.ordersservice.event.OrderCreatedEvent;
import com.jchaaban.ordersservice.event.OrderDeletedEvent;
import com.jchaaban.ordersservice.event.OrderRejectedEvent;
import com.jchaaban.ordersservice.repository.OrderEntity;
import com.jchaaban.ordersservice.repository.OrderRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.logging.Logger;

@Component
public class OrderEventHandler {

    private final OrderRepository orderRepository;

    public OrderEventHandler(OrderRepository productRepository) {
        this.orderRepository = productRepository;
    }

    private static final Logger logger = Logger.getLogger(OrderEventHandler.class.getName());

    @EventHandler
    public void on(OrderCreatedEvent orderCreatedEvent){
        logger.info("We are in OrderEventHandler in on OrderCreatedEvent");
        OrderEntity orderEntity = new OrderEntity(
                orderCreatedEvent.getUserId(),
                orderCreatedEvent.getOrderId(),
                orderCreatedEvent.getProductIds(),
                OrderStatus.CREATED,
                orderCreatedEvent.getOrderTotalPrice()
        );

        orderRepository.save(orderEntity);
    }

    @EventHandler
    public void on(OrderApprovedEvent orderApprovedEvent){
        logger.info("We are in OrderEventHandler in on OrderApprovedEvent");
        OrderEntity orderEntity = orderRepository.findById(orderApprovedEvent.getOrderId()).get();
        orderEntity.setOrderStatus(OrderStatus.APPROVED);
        orderEntity.setTotalPrice(orderApprovedEvent.getOrderTotalPrice());
        orderRepository.save(orderEntity);
    }
    @EventHandler
    public void on(OrderDeletedEvent orderDeletedEvent) {
        logger.info("We are in OrderEventHandler in on orderApprovedEvent");
        deleteOrder(orderDeletedEvent.getOrderId());
    }

    @EventHandler
    public void on(OrderRejectedEvent orderRejectedEvent) {
        logger.info("We are in OrderEventHandler in on OrderRejectedEvent");
        OrderEntity orderEntity = orderRepository.findById(orderRejectedEvent.getOrderId()).get();
            orderEntity.setOrderStatus(OrderStatus.REJECTED);
        orderRepository.save(orderEntity);
    }

    private void deleteOrder(String orderId) {
        Optional<OrderEntity> orderEntityOptional = orderRepository.findById(orderId);
        orderEntityOptional.ifPresent(orderRepository::delete);
    }


}
