package com.jchaaban.ordersservice.common;

import lombok.Data;

import java.util.UUID;

@Data
public class ProductToReserve {
    private final UUID productId;
    private final int quantity;
}
