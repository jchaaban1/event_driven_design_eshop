package com.jchaaban.ordersservice.config;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.AnyTypePermission;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class XStreamSecurityConfig {

    @Bean
    public XStream xStream() {
        XStream xStream = new XStream();

        // Configure XStream to allow only specific classes
        configureXStreamSecurity(xStream);

        return xStream;
    }

    private void configureXStreamSecurity(XStream xStream) {
        // Allow deserialization for specific packages or classes
        xStream.addPermission(new AnyTypePermission());

        // Whitelist specific packages or classes
        xStream.allowTypesByWildcard(new String[]{
                "com.jchaaban.common.*"
        });
    }
}
