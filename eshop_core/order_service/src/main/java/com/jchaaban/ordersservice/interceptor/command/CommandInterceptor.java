package com.jchaaban.ordersservice.interceptor.command;

import com.jchaaban.ordersservice.command.DeleteOrderCommand;
import com.jchaaban.ordersservice.repository.OrderRepository;
import com.jchaaban.ordersservice.rest.exception.OrderNotFoundException;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.BiFunction;

@Component
public class CommandInterceptor implements MessageDispatchInterceptor<CommandMessage<?>> {

    private final OrderRepository orderRepository;

    public CommandInterceptor(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Nonnull
    @Override
    public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(
            @Nonnull List<? extends CommandMessage<?>> list
    ) {

        return (index, command) -> {

            if (command.getPayloadType().equals(DeleteOrderCommand.class)){
                DeleteOrderCommand deleteOrderCommand = (DeleteOrderCommand) command.getPayload();
                if (orderRepository.findById(deleteOrderCommand.getOrderId()).isEmpty()){
                    throw new OrderNotFoundException(deleteOrderCommand.getOrderId());
                }
            }

            return command;
        };
    }


}
