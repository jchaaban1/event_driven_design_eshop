package com.jchaaban.ordersservice.event;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderTotalPriceIncrementedEvent {
    private final BigDecimal amount;
}
