package com.jchaaban.ordersservice.saga.service;

import com.jchaaban.common.command.PorcessPaymentCommand;
import com.jchaaban.common.dto.ReadPaymentDetailsDto;
import com.jchaaban.common.event.ProductReservedEvent;
import com.jchaaban.common.model.OrderStatus;
import com.jchaaban.ordersservice.command.ApproveOrderCommand;
import com.jchaaban.ordersservice.command.RejectOrderCommand;
import com.jchaaban.ordersservice.event.OrderCreatedEvent;
import com.jchaaban.ordersservice.saga.context.OrderContext;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.logging.Logger;

@Component
@RequiredArgsConstructor
public class OrderProcessingService {

    private static final Logger logger = Logger.getLogger(OrderProcessingService.class.getName());
    private static final String USER_NOT_FOUND_TEMPLATE = "User with ID: %s not found.";
    private static final String PAYMENT_DETAILS_NOT_FOUND = "Payment details for user with ID: %s not found.";
    private static final int SHIPPING_COST = 20;

    private final CommandGateway commandGateway;
    private final UserQueryService userQueryService;
    private final OrderSummaryNotifier orderSummaryEmitter;
    private final ProductReservationService productReservationService;
    private final ShipmentService shipmentService;

    private OrderContext orderContext;

    public void processOrderCreation(OrderCreatedEvent event) {
        initializeOrderContext(event);

        if (isUserAbsent()) {
            handleUserNotFound();
            return;
        }

        reserveProducts();
    }

    public void processProductReservation(ProductReservedEvent productReservedEvent) {
        orderContext.updateTotalPrice(productReservedEvent);
        orderContext.addReservedProduct(productReservedEvent);

        if (orderContext.allProductsReserved()) {
            sendProcessPaymentCommand();
        }
    }

    public void approveOrder() {
        logger.info("Payment approved for order " + orderContext.getOrderId());
        commandGateway.sendAndWait(new ApproveOrderCommand(
                orderContext.getOrderId(),
                OrderStatus.APPROVED,
                orderContext.getTotalPriceWithShipping()
        ));
    }

    public void processOrderApproval() {
        shipmentService.createShipment(
                orderContext.getOrderId(),
                orderContext.getUserId(),
                orderContext.getTotalPrice(),
                BigDecimal.valueOf(SHIPPING_COST)
        );
    }

    private void reserveProducts() {
        productReservationService.reserveProducts(
                orderContext.getProductsToReserve(),
                orderContext.getOrderId(),
                this::rejectOrder
        );
    }

    private void initializeOrderContext(OrderCreatedEvent event) {
        orderContext = new OrderContext(event);
    }

    private boolean isUserAbsent() {
        return !userQueryService.userExists(orderContext.getUserId());
    }

    private void handleUserNotFound() {
        rejectOrder(String.format(USER_NOT_FOUND_TEMPLATE, orderContext.getUserId()));
    }

    private void sendProcessPaymentCommand() {
        ReadPaymentDetailsDto paymentDetails = fetchUserPaymentDetails();

        boolean userDetailsNotFound = paymentDetails == null;

        if (userDetailsNotFound) {
            handleMissingPaymentDetails();
            return;
        }

        try {
            processPayment(paymentDetails);
        } catch (CommandExecutionException commandExecutionException) {
            handlePaymentCommandException(commandExecutionException);
        }
    }

    private ReadPaymentDetailsDto fetchUserPaymentDetails() {
        return userQueryService.fetchUserPaymentDetails(orderContext.getUserId());
    }

    private void handleMissingPaymentDetails() {
        rejectOrder(String.format(PAYMENT_DETAILS_NOT_FOUND, orderContext.getUserId()));
    }

    private void processPayment(ReadPaymentDetailsDto paymentDetails) throws CommandExecutionException {
        commandGateway.sendAndWait(new PorcessPaymentCommand(
                paymentDetails.getPaymentDetailsId(),
                orderContext.getOrderId(),
                orderContext.getTotalPriceWithShipping()
        ));
    }

    private void handlePaymentCommandException(CommandExecutionException commandExecutionException) {
        rejectOrder(commandExecutionException.getMessage());
    }

    private void rejectOrder(String reason) {
        logger.info("Rejecting order");
        sendRejectOrderCommand(reason);
        rollbackProductReservations();
        emitOrderSummary(reason);
    }

    private void sendRejectOrderCommand(String reason) {
        commandGateway.send(new RejectOrderCommand(orderContext.getOrderId(), reason));
    }

    private void rollbackProductReservations() {
        productReservationService.rollbackProductsReservation(orderContext.getReservedProducts(), orderContext.getOrderId());
    }

    private void emitOrderSummary(String reason) {
        orderSummaryEmitter.emitOrderSummary(orderContext.getOrderId(), reason);
    }
}
