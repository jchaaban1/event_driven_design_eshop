package com.jchaaban.productservice.rest.controller.query;

import com.jchaaban.productservice.query.FetchProductQuery;
import com.jchaaban.productservice.query.FetchProductsQuery;
import com.jchaaban.productservice.rest.dto.ReadProductDto;
import com.jchaaban.productservice.rest.exception.ProductServiceErrorResponse;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryExecutionException;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
public class ProductQueryController {
    private final QueryGateway queryGateWay;

    public ProductQueryController(QueryGateway queryGateWay) {
        this.queryGateWay = queryGateWay;
    }
    @GetMapping("/products")
    public List<ReadProductDto> getProducts() {
        FetchProductsQuery productsQuery = new FetchProductsQuery();
        return queryGateWay.query(
                productsQuery,
                ResponseTypes.multipleInstancesOf(ReadProductDto.class)
        ).join();
    }

    @GetMapping("/product/{productId}")
    public ReadProductDto getProduct(@PathVariable String productId) {
        FetchProductQuery productQuery = new FetchProductQuery(productId);
        return queryGateWay.query(
                productQuery,
                ResponseTypes.instanceOf(ReadProductDto.class)
        ).join();
    }

    @ExceptionHandler(QueryExecutionException.class)
    public ResponseEntity<ProductServiceErrorResponse> handleQueryExecution(
            QueryExecutionException queryExecutionException
    ) {

        String errorMessage = queryExecutionException.getMessage();
        ProductServiceErrorResponse errorResponse = new ProductServiceErrorResponse(errorMessage);

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}
