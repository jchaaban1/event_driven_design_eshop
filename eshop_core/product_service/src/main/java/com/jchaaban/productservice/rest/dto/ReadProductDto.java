package com.jchaaban.productservice.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class ReadProductDto {

    @JsonProperty(required = true)
    private String productId;

    @JsonProperty(required = true)
    private String title;

    @JsonProperty(required = true)
    private BigDecimal price;

    @JsonProperty(required = true)
    private Integer quantity;
}
