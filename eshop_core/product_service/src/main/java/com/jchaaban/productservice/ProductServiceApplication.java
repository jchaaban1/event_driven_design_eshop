package com.jchaaban.productservice;

import com.jchaaban.productservice.interceptor.command.CommandInterceptor;
import com.jchaaban.productservice.config.XStreamSecurityConfig;
import com.jchaaban.productservice.interceptor.query.QueryInterceptor;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.queryhandling.QueryBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

@EnableDiscoveryClient
@SpringBootApplication
@Import({ XStreamSecurityConfig.class })
public class ProductServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductServiceApplication.class, args);
    }

    @Autowired
    public void registerInterceptors(
            ApplicationContext context,
            CommandBus commandBus,
            QueryBus queryBus
    ){
        commandBus.registerDispatchInterceptor(context.getBean(CommandInterceptor.class));
        queryBus.registerHandlerInterceptor(context.getBean(QueryInterceptor.class));
    }

}
