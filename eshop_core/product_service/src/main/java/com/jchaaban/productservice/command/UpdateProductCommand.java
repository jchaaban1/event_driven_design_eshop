package com.jchaaban.productservice.command;

import com.jchaaban.productservice.rest.dto.ReadProductDto;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class UpdateProductCommand extends ProductCommand {

    private final String title;
    private final int quantity;
    private final BigDecimal price;

    public UpdateProductCommand(String productId, String title, int quantity, BigDecimal price) {
        super(productId);
        this.title = title;
        this.quantity = quantity;
        this.price = price;
    }

    public ReadProductDto toToReadProductDto() {
        return new ReadProductDto(
                productId,
                title,
                price,
                quantity
        );
    }
}
