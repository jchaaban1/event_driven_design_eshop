package com.jchaaban.productservice.query;

import lombok.Data;

@Data
public class FetchProductQuery {
    private final String productId;
}
