package com.jchaaban.productservice.rest.exception;

import lombok.Getter;

@Getter
public class ProductServiceException extends RuntimeException {
    public ProductServiceException(String message) {
        super(message);
    }

}