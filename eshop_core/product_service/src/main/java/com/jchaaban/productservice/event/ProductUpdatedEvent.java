package com.jchaaban.productservice.event;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductUpdatedEvent {
    private final String productId;
    private final String title;
    private final Integer quantity;
    private final BigDecimal price;
}