package com.jchaaban.productservice.rest.exception;

public class DuplicatedTitleException extends ProductServiceException {
    public DuplicatedTitleException(String message) {
        super(message);
    }
}
