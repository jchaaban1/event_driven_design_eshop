package com.jchaaban.productservice.aggregate;

import com.jchaaban.common.command.ReserveProductCommand;
import com.jchaaban.common.command.UnreserveProductCommand;
import com.jchaaban.common.event.ProductReservedEvent;
import com.jchaaban.common.event.ProductUnreservedEvent;
import com.jchaaban.productservice.aggregate.exception.InsufficientQuantityException;
import com.jchaaban.productservice.command.CreateProductCommand;
import com.jchaaban.productservice.command.DeleteProductCommand;
import com.jchaaban.productservice.command.UpdateProductCommand;
import com.jchaaban.productservice.event.ProductCreatedEvent;
import com.jchaaban.productservice.event.ProductDeletedEvent;
import com.jchaaban.productservice.event.ProductUpdatedEvent;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.logging.Logger;


@Aggregate
@NoArgsConstructor
@Component
public class ProductAggregate {

    @AggregateIdentifier
    private String productId;
    private String title;
    private Integer quantity;
    private BigDecimal price;

    private static final Logger logger = Logger.getLogger(ProductAggregate.class.getName());


    /* In Axon Framework, creating an aggregate typically involves calling a constructor with a @CommandHandler
        annotation. This is because when an aggregate is created, it is expected to generate its first event and apply
        it. The AggregateIdentifier is expected to be set in this creation event since it is used to retrieve the
        aggregate from the event store.
    */
    @CommandHandler
    public ProductAggregate(CreateProductCommand createProductCommand) {
        ProductCreatedEvent productCreatedEvent = new ProductCreatedEvent(
                createProductCommand.getProductId(),
                createProductCommand.getTitle(),
                createProductCommand.getQuantity(),
                createProductCommand.getPrice()
        );
        AggregateLifecycle.apply(productCreatedEvent);
    }

    @CommandHandler
    public void handle(UpdateProductCommand updateProductCommand) {
        ProductUpdatedEvent productUpdatedEvent = new ProductUpdatedEvent(
                updateProductCommand.getProductId(),
                updateProductCommand.getTitle(),
                updateProductCommand.getQuantity(),
                updateProductCommand.getPrice()
        );
        AggregateLifecycle.apply(productUpdatedEvent);
    }

    @CommandHandler
    public void handle(DeleteProductCommand deleteProductCommand) {
        ProductDeletedEvent productDeletedEvent = new ProductDeletedEvent(deleteProductCommand.getProductId());
        AggregateLifecycle.apply(productDeletedEvent);
    }

    @CommandHandler
    public void handle(ReserveProductCommand reserveProductsCommand) {
        logger.info("In ProductAggregate handle ReserveProductCommand");
        if (this.quantity < reserveProductsCommand.getQuantity()) {
            throw new InsufficientQuantityException(
                    "Insufficient number of items in stock for the product having ID:" + productId
            );
        }

        ProductReservedEvent productReservedEvent = new ProductReservedEvent(
                reserveProductsCommand.getProductId(),
                reserveProductsCommand.getOrderId(),
                reserveProductsCommand.getQuantity(),
                price
        );

        AggregateLifecycle.apply(productReservedEvent);
    }

    @CommandHandler
    public void handle(UnreserveProductCommand unreserveProductCommand) {
        logger.info("In ProductAggregate handle UnreserveProductCommand");

        ProductUnreservedEvent productUnreservedEvent = new ProductUnreservedEvent(
                unreserveProductCommand.getOrderId(),
                unreserveProductCommand.getProductId(),
                unreserveProductCommand.getQuantity(),
                price
        );

        AggregateLifecycle.apply(productUnreservedEvent);
    }

    @EventSourcingHandler
    public void on(ProductUnreservedEvent productUnreservedEvent) {
        logger.info("We are in on " + productUnreservedEvent.getClass().getName());
        quantity += productUnreservedEvent.getQuantity();
    }

    @EventSourcingHandler
    public void on(ProductUpdatedEvent productEvent) {
        logger.info("We are in on " + productEvent.getClass().getName());
        productId = productEvent.getProductId();
        title = productEvent.getTitle();
        quantity = productEvent.getQuantity();
        price = productEvent.getPrice();
    }

    @EventSourcingHandler
    public void on(ProductDeletedEvent productEvent) {
        logger.info("We are in on " + productEvent.getClass().getName());
        productId = productEvent.getProductId();
    }

    @EventSourcingHandler
    public void on(ProductCreatedEvent productEvent) {
        logger.info("We are in on " + productEvent.getClass().getName());
        productId = productEvent.getProductId();
        title = productEvent.getTitle();
        quantity = productEvent.getQuantity();
        price = productEvent.getPrice();
    }

    @EventSourcingHandler
    public void on(ProductReservedEvent productReservedEvent) {
        logger.info("We are in on " + productReservedEvent.getClass().getName());
        quantity -= productReservedEvent.getQuantity();
    }
}
