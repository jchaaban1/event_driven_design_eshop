package com.jchaaban.productservice.config;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.AnyTypePermission;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class XStreamSecurityConfig {

    @Bean
    public XStream xStream() {
        XStream xStream = new XStream();

        configureXStreamSecurity(xStream);

        return xStream;
    }

    private void configureXStreamSecurity(XStream xStream) {
        xStream.addPermission(new AnyTypePermission());

        xStream.allowTypesByWildcard(new String[]{
                "com.jchaaban.common.*"
        });
    }

}
