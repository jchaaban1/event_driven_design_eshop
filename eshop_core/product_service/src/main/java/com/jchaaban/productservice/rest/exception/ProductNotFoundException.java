package com.jchaaban.productservice.rest.exception;
public class ProductNotFoundException extends ProductServiceException {
    public ProductNotFoundException(String productId) {
        super("The product with the id " + productId + " doesn't exist");
    }
}
