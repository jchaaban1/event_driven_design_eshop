package com.jchaaban.productservice.aggregate.exception;

public class InsufficientQuantityException extends RuntimeException {
    public InsufficientQuantityException(String productId) {
        super("Insufficient quantity for product with ID: " + productId);
    }
}
