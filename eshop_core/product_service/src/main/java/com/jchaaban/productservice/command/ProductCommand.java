package com.jchaaban.productservice.command;

import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class ProductCommand {

    @TargetAggregateIdentifier
    protected final String productId;
}
