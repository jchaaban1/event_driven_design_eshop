package com.jchaaban.productservice.rest.controller.command;

import com.jchaaban.productservice.command.CreateProductCommand;
import com.jchaaban.productservice.command.DeleteProductCommand;
import com.jchaaban.productservice.command.UpdateProductCommand;
import com.jchaaban.productservice.rest.dto.CreateProductDto;
import com.jchaaban.productservice.rest.dto.ReadProductDto;
import com.jchaaban.productservice.rest.dto.UpdateProductDto;
import com.jchaaban.productservice.rest.exception.DuplicatedTitleException;
import com.jchaaban.productservice.rest.exception.ProductInvalidArgumentsException;
import com.jchaaban.productservice.rest.exception.ProductNotFoundException;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/products")
public class ProductCommandController {
    private final CommandGateway commandGateway;

    public ProductCommandController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PostMapping
    public ResponseEntity<ReadProductDto> createProduct(@RequestBody CreateProductDto productDto) {
        String productId = UUID.randomUUID().toString();
        CreateProductCommand createProductCommand = new CreateProductCommand(
                productId,
                productDto.getTitle(),
                productDto.getQuantity(),
                productDto.getPrice()
        );

        commandGateway.sendAndWait(createProductCommand);

        return new ResponseEntity<>(
                createProductCommand.toToReadProductDto(), HttpStatus.OK
        );
    }

    @PutMapping("/{productId}")
    public ResponseEntity<ReadProductDto> updateProduct(@PathVariable String productId, @RequestBody UpdateProductDto updateProductDto) {
        UpdateProductCommand updateProductCommand = new UpdateProductCommand(
                productId,
                updateProductDto.getTitle(),
                updateProductDto.getQuantity(),
                updateProductDto.getPrice()
        );

        commandGateway.sendAndWait(updateProductCommand);

        return new ResponseEntity<>(
                updateProductCommand.toToReadProductDto(), HttpStatus.OK
        );
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<String> deleteProduct(@PathVariable String productId) {

        DeleteProductCommand deleteProductCommand = new DeleteProductCommand(productId);

        commandGateway.sendAndWait(deleteProductCommand);

        return new ResponseEntity<>(
                "Product was successfully deleted", HttpStatus.OK
        );
    }

    @ExceptionHandler(DuplicatedTitleException.class)
    public ResponseEntity<String> handleDuplicatedTitleException(
            DuplicatedTitleException duplicatedTitleException
    ) {
        return new ResponseEntity<>(duplicatedTitleException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ProductInvalidArgumentsException.class)
    public ResponseEntity<String> handleProductInvalidArgumentsException(
            ProductInvalidArgumentsException productInvalidArgumentsException
    ) {
        return new ResponseEntity<>(productInvalidArgumentsException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<String> handleProductNotFoundException(
            ProductNotFoundException productNotFoundException
    ) {
        return new ResponseEntity<>(productNotFoundException.getMessage(), HttpStatus.NOT_FOUND);
    }

}
