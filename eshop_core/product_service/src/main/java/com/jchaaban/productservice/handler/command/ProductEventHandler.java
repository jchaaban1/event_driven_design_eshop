package com.jchaaban.productservice.handler.command;

import com.jchaaban.common.event.ProductReservedEvent;
import com.jchaaban.common.event.ProductUnreservedEvent;
import com.jchaaban.productservice.event.ProductCreatedEvent;
import com.jchaaban.productservice.event.ProductDeletedEvent;
import com.jchaaban.productservice.event.ProductUpdatedEvent;
import com.jchaaban.productservice.repository.ProductEntity;
import com.jchaaban.productservice.repository.ProductRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class ProductEventHandler {

    private final ProductRepository productRepository;

    private static final Logger logger = Logger.getLogger(ProductEventHandler.class.getName());
    public ProductEventHandler(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @EventHandler
    public void on(ProductCreatedEvent productCreatedEvent){
        ProductEntity productEntity = new ProductEntity(
                productCreatedEvent.getProductId(),
                productCreatedEvent.getTitle(),
                productCreatedEvent.getQuantity(),
                productCreatedEvent.getPrice()
        );

        productRepository.save(productEntity);
    }

    @EventHandler
    public void on(ProductUpdatedEvent productUpdatedEvent) {
        productRepository.findById(productUpdatedEvent.getProductId()).ifPresent(productEntity -> {
            productEntity.setTitle(productUpdatedEvent.getTitle());
            productEntity.setQuantity(productUpdatedEvent.getQuantity());
            productEntity.setPrice(productUpdatedEvent.getPrice());
            productRepository.save(productEntity);
        });
    }
    @EventHandler
    public void on(ProductDeletedEvent productDeletedEvent) {
        productRepository.findById(productDeletedEvent.getProductId()).ifPresent(productRepository::delete);
    }

    @EventHandler
    public void on(ProductReservedEvent productsReservedEvent) {
        logger.info("we are trying to reserve " + productsReservedEvent.getProductId());
        ProductEntity product = productRepository.findByProductId(productsReservedEvent.getProductId());

        Integer currentQuantity = product.getQuantity();
        product.setQuantity(currentQuantity - productsReservedEvent.getQuantity());
        productRepository.save(product);
    }

    @EventHandler
    public void on(ProductUnreservedEvent productUnreservedEvent) {
        ProductEntity product = productRepository.findByProductId(productUnreservedEvent.getProductId());

        Integer currentQuantity = product.getQuantity();
        product.setQuantity(currentQuantity + productUnreservedEvent.getQuantity());
        productRepository.save(product);
    }
}
