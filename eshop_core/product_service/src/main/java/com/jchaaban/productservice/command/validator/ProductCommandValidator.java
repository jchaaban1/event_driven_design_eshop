package com.jchaaban.productservice.command.validator;

import com.jchaaban.productservice.command.CreateProductCommand;
import com.jchaaban.productservice.command.ProductCommand;
import com.jchaaban.productservice.command.UpdateProductCommand;
import com.jchaaban.productservice.repository.ProductEntity;
import com.jchaaban.productservice.repository.ProductRepository;
import com.jchaaban.productservice.rest.exception.DuplicatedTitleException;
import com.jchaaban.productservice.rest.exception.ProductInvalidArgumentsException;
import com.jchaaban.productservice.rest.exception.ProductNotFoundException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.function.Predicate;

@Component
public class ProductCommandValidator {

    private static final String PRODUCT_NOT_FOUND = "Product not found";
    private static final String DUPLICATED_TITLE = "This product title is already used by another product";
    private static final String TITLE_EMPTY = "Title must not be empty";
    private static final String PRICE_NEGATIVE = "The value of price must be positive";
    private static final String QUANTITY_NEGATIVE = "The value of quantity must be positive";

    private static final Predicate<String> NOT_EMPTY = s -> s != null && !s.trim().isEmpty();
    private static final Predicate<BigDecimal> POSITIVE_PRICE = price -> price.compareTo(BigDecimal.ZERO) > 0;
    private static final Predicate<Integer> POSITIVE_QUANTITY = quantity -> quantity > 0;

    protected final ProductRepository productRepository;

    public ProductCommandValidator(ProductRepository repository) {
        this.productRepository = repository;
    }

    public void validate(ProductCommand productCommand) {
        if (productCommand instanceof CreateProductCommand) {
            validateCreateProductCommand((CreateProductCommand) productCommand);
        } else if (productCommand instanceof UpdateProductCommand) {
            validateUpdateProductCommand((UpdateProductCommand) productCommand);
        } else {
            validateProductIdExists(productCommand.getProductId());
        }
    }

    private void validateUpdateProductCommand(UpdateProductCommand updateProductCommand) {
        validateProductProperties(
                updateProductCommand.getTitle(),
                updateProductCommand.getQuantity(),
                updateProductCommand.getPrice()
        );

        validateProductIdExists(updateProductCommand.getProductId());
        validateUniqueTitle(updateProductCommand);
    }

    private void validateCreateProductCommand(CreateProductCommand createProductCommand) {
        validateProductProperties(
                createProductCommand.getTitle(),
                createProductCommand.getQuantity(),
                createProductCommand.getPrice()
        );

        validateUniqueTitle(createProductCommand);
    }

    private void validateProductProperties(String title, int quantity, BigDecimal price) {
        validateField(NOT_EMPTY, title, TITLE_EMPTY);
        validateField(POSITIVE_QUANTITY, quantity, QUANTITY_NEGATIVE);
        validateField(POSITIVE_PRICE, price, PRICE_NEGATIVE);
    }

    private void validateUniqueTitle(UpdateProductCommand updateCommand) {
        ProductEntity product = productRepository.findByTitle(updateCommand.getTitle());
        if (product != null && !product.getProductId().equals(updateCommand.getProductId())) {
            throw new DuplicatedTitleException(DUPLICATED_TITLE);
        }
    }

    private void validateUniqueTitle(CreateProductCommand createProductCommand) {
        if (productRepository.findByTitle(createProductCommand.getTitle()) != null) {
            throw new DuplicatedTitleException(DUPLICATED_TITLE);
        }
    }

    private void validateProductIdExists(String productId) {
        if (productRepository.findByProductId(productId) == null) {
            throw new ProductNotFoundException(PRODUCT_NOT_FOUND);
        }
    }

    private <T> void validateField(Predicate<T> predicate, T field, String errorMessage) {
        if (!predicate.test(field)) {
            throw new ProductInvalidArgumentsException(errorMessage);
        }
    }
}
