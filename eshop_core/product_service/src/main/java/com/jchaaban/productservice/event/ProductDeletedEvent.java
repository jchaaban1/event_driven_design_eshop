package com.jchaaban.productservice.event;

import lombok.Data;

@Data
public class ProductDeletedEvent {
    private final String productId;
}