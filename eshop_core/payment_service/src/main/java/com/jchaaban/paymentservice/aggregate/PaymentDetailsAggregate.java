package com.jchaaban.paymentservice.aggregate;


import com.jchaaban.common.command.PorcessPaymentCommand;
import com.jchaaban.common.event.PaymentProcessedEvent;
import com.jchaaban.paymentservice.command.CreatePaymentDetailsCommand;
import com.jchaaban.paymentservice.command.DeletePaymentDetailsCommand;
import com.jchaaban.paymentservice.command.UpdatePaymentDetailsCommand;
import com.jchaaban.paymentservice.event.PaymentDetailsCreatedEvent;
import com.jchaaban.paymentservice.event.PaymentDetailsDeletedEvent;
import com.jchaaban.paymentservice.event.PaymentDetailsUpdatedEvent;
import com.jchaaban.paymentservice.saga.exception.InsufficientFundsException;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.logging.Logger;

@Aggregate
@NoArgsConstructor
@Component
public class PaymentDetailsAggregate {

    @AggregateIdentifier
    private String paymentDetailsId;
    private String userId;
    private String cardNumber;
    private int validUntilMonth;
    private int validUntilYear;
    private String cvv;
    private BigDecimal balance;

    private static final Logger logger = Logger.getLogger(PaymentDetailsAggregate.class.getName());


    @CommandHandler
    public PaymentDetailsAggregate(CreatePaymentDetailsCommand createPaymentDetailsCommand) {
        logger.info("We are in on " + createPaymentDetailsCommand.getClass().getName());
        PaymentDetailsCreatedEvent paymentDetailsCreatedEvent = new PaymentDetailsCreatedEvent(
                createPaymentDetailsCommand.getPaymentDetailsId(),
                createPaymentDetailsCommand.getUserId(),
                createPaymentDetailsCommand.getCardNumber(),
                createPaymentDetailsCommand.getValidUntilMonth(),
                createPaymentDetailsCommand.getValidUntilYear(),
                createPaymentDetailsCommand.getCvv(),
                createPaymentDetailsCommand.getBalance());
        AggregateLifecycle.apply(paymentDetailsCreatedEvent);
    }

    @EventSourcingHandler
    public void on(PaymentDetailsCreatedEvent paymentDetailsCreatedEvent) {
        logger.info("We are in on " + paymentDetailsCreatedEvent.getClass().getName());
        paymentDetailsId = paymentDetailsCreatedEvent.getPaymentDetailsId();
        userId = paymentDetailsCreatedEvent.getUserId();
        cardNumber = paymentDetailsCreatedEvent.getCardNumber();
        validUntilMonth = paymentDetailsCreatedEvent.getValidUntilMonth();
        validUntilYear = paymentDetailsCreatedEvent.getValidUntilYear();
        cvv = paymentDetailsCreatedEvent.getCvv();
        balance = paymentDetailsCreatedEvent.getBalance();
    }
    @CommandHandler
    public void handle(UpdatePaymentDetailsCommand updatePaymentDetailsCommand) {
        logger.info("We are in on " + updatePaymentDetailsCommand.getClass().getName());
        PaymentDetailsCreatedEvent paymentDetailsCreatedEvent = new PaymentDetailsCreatedEvent(
                updatePaymentDetailsCommand.getPaymentDetailsId(),
                updatePaymentDetailsCommand.getUserId(),
                updatePaymentDetailsCommand.getCardNumber(),
                updatePaymentDetailsCommand.getValidUntilMonth(),
                updatePaymentDetailsCommand.getValidUntilYear(),
                updatePaymentDetailsCommand.getCvv(),
                updatePaymentDetailsCommand.getBalance()
        );
        AggregateLifecycle.apply(paymentDetailsCreatedEvent);
    }

    @EventSourcingHandler
    public void on(PaymentDetailsUpdatedEvent paymentDetailsUpdatedEvent) {
        logger.info("We are in on " + paymentDetailsUpdatedEvent.getClass().getName());
        paymentDetailsId = paymentDetailsUpdatedEvent.getPaymentDetailsId();
        userId = paymentDetailsUpdatedEvent.getUserId();
        cardNumber = paymentDetailsUpdatedEvent.getCardNumber();
        validUntilMonth = paymentDetailsUpdatedEvent.getValidUntilMonth();
        validUntilYear = paymentDetailsUpdatedEvent.getValidUntilYear();
        cvv = paymentDetailsUpdatedEvent.getCvv();
        balance = paymentDetailsUpdatedEvent.getBalance();
    }
    @CommandHandler
    public void handle(DeletePaymentDetailsCommand deletePaymentDetailsCommand) {
        logger.info("We are in on " + deletePaymentDetailsCommand.getClass().getName());
        PaymentDetailsDeletedEvent paymentDetailsDeletedEvent = new PaymentDetailsDeletedEvent(
                deletePaymentDetailsCommand.getPaymentDetailsId()
        );
        AggregateLifecycle.apply(paymentDetailsDeletedEvent);
    }

    @EventSourcingHandler
    public void on(PaymentDetailsDeletedEvent paymentDetailsDeletedEvent) {
        logger.info("We are in on " + paymentDetailsDeletedEvent.getClass().getName());
        paymentDetailsId = paymentDetailsDeletedEvent.getPaymentDetailsId();
    }

    @CommandHandler
    public void handle(PorcessPaymentCommand porcessPaymentCommand) {
        logger.info("We are in on " + porcessPaymentCommand.getClass().getName());

        if (porcessPaymentCommand.getAmountToSubtract().compareTo(balance) > 0){
            throw new InsufficientFundsException("Insufficient balance.");
        }

        PaymentProcessedEvent paymentDetailsBalanceUpdatedEvent = new PaymentProcessedEvent(
                paymentDetailsId,
                porcessPaymentCommand.getOrderId(),
                porcessPaymentCommand.getAmountToSubtract()
        );
        AggregateLifecycle.apply(paymentDetailsBalanceUpdatedEvent);
    }

    @EventSourcingHandler
    public void on(PaymentProcessedEvent paymentDetailsBalanceUpdatedEvent) {
        logger.info("We are in on " + paymentDetailsBalanceUpdatedEvent.getClass().getName());
        balance = balance.subtract(paymentDetailsBalanceUpdatedEvent.getOrderTotalPrice());
    }

}
