package com.jchaaban.paymentservice.query;

import lombok.Data;

@Data
public class FetchPaymentDetailsQuery {
    private final String paymentDetailsId;
}
