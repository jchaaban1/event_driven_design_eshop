package com.jchaaban.paymentservice.command;

import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class PaymentDetailsCommand {

    @TargetAggregateIdentifier
    protected final String paymentDetailsId;
}
