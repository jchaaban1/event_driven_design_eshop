package com.jchaaban.paymentservice.event;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PaymentDetailsCreatedEvent {

    private final String paymentDetailsId;
    private final String userId;
    private final String cardNumber;
    private final int validUntilMonth;
    private final int validUntilYear;
    private final String cvv;
    private final BigDecimal balance;
}
