package com.jchaaban.paymentservice.command.validator;

import com.jchaaban.paymentservice.command.CreatePaymentDetailsCommand;
import com.jchaaban.paymentservice.command.PaymentDetailsCommand;
import com.jchaaban.paymentservice.command.UpdatePaymentDetailsCommand;
import com.jchaaban.paymentservice.repository.PaymentDetailsEntity;
import com.jchaaban.paymentservice.repository.PaymentDetailsRepository;
import com.jchaaban.paymentservice.rest.exception.DuplicateCardNumberException;
import com.jchaaban.paymentservice.rest.exception.PaymentDetailsInvalidArgumentsException;
import com.jchaaban.paymentservice.rest.exception.PaymentDetailsNotFoundException;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

@Component
public class PaymentDetailsCommandValidator {
    private static final String PAYMENT_DETAILS_NOT_FOUND = "Payment details not found";
    private static final String DUPLICATE_PAYMENT_DETAILS = "Payment details already exist";
    private static final String FIELD_EMPTY = "Field must not be empty";
    private static final String CARD_NUMBER_INVALID = "Invalid card number";
    private static final String CVV_INVALID = "Invalid CVV";
    private static final String VALID_UNTIL_MONTH_INVALID = "Invalid valid until month";
    private static final String VALID_UNTIL_YEAR_INVALID = "Invalid valid until year";

    private static final Predicate<String> NOT_EMPTY = string -> string != null && !string.trim().isEmpty();
    private static final Predicate<String> VALID_CARD_NUMBER = cardNumber -> cardNumber != null && cardNumber.matches("[0-9]{16}");
    private static final Predicate<String> VALID_CVV = csv -> csv != null && csv.matches("[0-9]{3}");
    private static final Predicate<Integer> VALID_UNTIL_MONTH = month -> month >= 1 && month <= 12;
    private static final Predicate<Integer> VALID_UNTIL_YEAR = year -> year >= 2023;

    private final PaymentDetailsRepository paymentDetailsRepository;

    public PaymentDetailsCommandValidator(PaymentDetailsRepository paymentDetailsRepository) {
        this.paymentDetailsRepository = paymentDetailsRepository;
    }

    public void validate(PaymentDetailsCommand command) {
        if (command instanceof CreatePaymentDetailsCommand) {
            validateCreatePaymentDetailsCommand((CreatePaymentDetailsCommand) command);
        } else if (command instanceof UpdatePaymentDetailsCommand) {
            validateUpdatePaymentDetailsCommand((UpdatePaymentDetailsCommand) command);
        } else {
            validatePaymentDetailsIdExists(command.getPaymentDetailsId());
        }
    }

    private void validateUpdatePaymentDetailsCommand(UpdatePaymentDetailsCommand command) {
        validatePaymentDetailsProperties(
                command.getUserId(),
                command.getCardNumber(),
                command.getValidUntilMonth(),
                command.getValidUntilYear(),
                command.getCvv()
        );

        validatePaymentDetailsIdExists(command.getPaymentDetailsId());
        validateUniqueCardNumber(command);

    }

    private void validateCreatePaymentDetailsCommand(CreatePaymentDetailsCommand command) {
        validatePaymentDetailsProperties(
                command.getUserId(),
                command.getCardNumber(),
                command.getValidUntilMonth(),
                command.getValidUntilYear(),
                command.getCvv()
        );

        validateUniqueCardNumber(command);
    }

    private void validatePaymentDetailsProperties(
            String userId,
            String cardNumber,
            int validUntilMonth,
            int validUntilYear,
            String cvv
    ) {
        validateField(NOT_EMPTY, userId, FIELD_EMPTY);
        validateField(VALID_CARD_NUMBER, cardNumber, CARD_NUMBER_INVALID);
        validateField(VALID_CVV, cvv, CVV_INVALID);
        validateField(VALID_UNTIL_MONTH, validUntilMonth, VALID_UNTIL_MONTH_INVALID);
        validateField(VALID_UNTIL_YEAR, validUntilYear, VALID_UNTIL_YEAR_INVALID);
    }

    private void validateUniqueCardNumber(CreatePaymentDetailsCommand command) {
        PaymentDetailsEntity paymentDetails = paymentDetailsRepository.findByCardNumber(command.getCardNumber());
        if (paymentDetails != null) {
            throw new DuplicateCardNumberException(DUPLICATE_PAYMENT_DETAILS);
        }
    }
    private void validateUniqueCardNumber(UpdatePaymentDetailsCommand command) {
        PaymentDetailsEntity paymentDetails = paymentDetailsRepository.findByCardNumber(command.getCardNumber());
        if (paymentDetails != null && !paymentDetails.getPaymentDetailsId().equals(command.getPaymentDetailsId())) {
            throw new DuplicateCardNumberException(DUPLICATE_PAYMENT_DETAILS);
        }
    }

    private void validatePaymentDetailsIdExists(String paymentDetailsId) {
        PaymentDetailsEntity paymentDetails = paymentDetailsRepository.findByPaymentDetailsId(paymentDetailsId);
        if (paymentDetails == null) {
            throw new PaymentDetailsNotFoundException(PAYMENT_DETAILS_NOT_FOUND);
        }
    }

    private <T> void validateField(Predicate<T> predicate, T field, String errorMessage) {
        if (!predicate.test(field)) {
            throw new PaymentDetailsInvalidArgumentsException(errorMessage);
        }
    }
}