package com.jchaaban.paymentservice.command;

import com.jchaaban.common.dto.ReadPaymentDetailsDto;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class UpdatePaymentDetailsCommand extends PaymentDetailsCommand {

    private final String userId;
    private final String cardNumber;
    private final int validUntilMonth;
    private final int validUntilYear;
    private final String cvv;
    private final BigDecimal balance;

    public UpdatePaymentDetailsCommand(
            String paymentDetailsId,
            String userId,
            String cardNumber,
            int validUntilMonth,
            int validUntilYear,
            String cvv,
            BigDecimal balance) {
        super(paymentDetailsId);
        this.userId = userId;
        this.cardNumber = cardNumber;
        this.validUntilMonth = validUntilMonth;
        this.validUntilYear = validUntilYear;
        this.cvv = cvv;
        this.balance = balance;
    }

    public ReadPaymentDetailsDto toToReadProductDto() {
        return new ReadPaymentDetailsDto(
                this.getPaymentDetailsId(),
                userId,
                cardNumber,
                validUntilMonth,
                validUntilYear,
                cvv,
                balance
        );
    }
}
