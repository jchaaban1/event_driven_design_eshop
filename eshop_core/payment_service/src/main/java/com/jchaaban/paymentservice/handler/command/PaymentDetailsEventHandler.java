package com.jchaaban.paymentservice.handler.command;

import com.jchaaban.common.event.PaymentProcessedEvent;
import com.jchaaban.paymentservice.aggregate.PaymentDetailsAggregate;
import com.jchaaban.paymentservice.event.PaymentDetailsCreatedEvent;
import com.jchaaban.paymentservice.event.PaymentDetailsDeletedEvent;
import com.jchaaban.paymentservice.event.PaymentDetailsUpdatedEvent;
import com.jchaaban.paymentservice.repository.PaymentDetailsEntity;
import com.jchaaban.paymentservice.repository.PaymentDetailsRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;


@Component
public class PaymentDetailsEventHandler {
    private final PaymentDetailsRepository paymentDetailsRepository;
    private static final Logger logger = Logger.getLogger(PaymentDetailsAggregate.class.getName());

    public PaymentDetailsEventHandler(PaymentDetailsRepository productRepository) {
        this.paymentDetailsRepository = productRepository;
    }



    @EventHandler
    public void on(PaymentDetailsCreatedEvent paymentDetailsCreatedEvent) {

        PaymentDetailsEntity paymentDetailsEntity = new PaymentDetailsEntity(
                paymentDetailsCreatedEvent.getPaymentDetailsId(),
                paymentDetailsCreatedEvent.getUserId(),
                paymentDetailsCreatedEvent.getCardNumber(),
                paymentDetailsCreatedEvent.getValidUntilMonth(),
                paymentDetailsCreatedEvent.getValidUntilYear(),
                paymentDetailsCreatedEvent.getCvv(),
                paymentDetailsCreatedEvent.getBalance()
        );

        paymentDetailsRepository.save(paymentDetailsEntity);
    }

    @EventHandler
    public void on(PaymentDetailsUpdatedEvent paymentDetailsUpdatedEvent) {

        paymentDetailsRepository.findById(paymentDetailsUpdatedEvent.getPaymentDetailsId()).ifPresent(
                existingUserEntity -> {
                    existingUserEntity.setPaymentDetailsId(paymentDetailsUpdatedEvent.getUserId());
                    existingUserEntity.setUserId(paymentDetailsUpdatedEvent.getUserId());
                    existingUserEntity.setCardNumber(paymentDetailsUpdatedEvent.getCardNumber());
                    existingUserEntity.setValidUntilMonth(paymentDetailsUpdatedEvent.getValidUntilMonth());
                    existingUserEntity.setValidUntilYear(paymentDetailsUpdatedEvent.getValidUntilYear());
                    existingUserEntity.setCvv(paymentDetailsUpdatedEvent.getCvv());
                    existingUserEntity.setBalance(paymentDetailsUpdatedEvent.getBalance());
                    paymentDetailsRepository.save(existingUserEntity);
                });
    }
    @EventHandler
    public void on(PaymentDetailsDeletedEvent paymentDetailsDeletedEvent) {
        paymentDetailsRepository.deleteById(paymentDetailsDeletedEvent.getPaymentDetailsId());
    }


    @EventHandler
    public void on(PaymentProcessedEvent paymentDetailsBalanceUpdatedEvent) {
        logger.info("We are in on " + paymentDetailsBalanceUpdatedEvent.getClass().getName());
        PaymentDetailsEntity paymentDetailsEntity = paymentDetailsRepository.findByPaymentDetailsId(
                paymentDetailsBalanceUpdatedEvent.getPaymentDetailsId()
        );

        paymentDetailsEntity.setBalance(
                paymentDetailsEntity.getBalance().subtract(paymentDetailsBalanceUpdatedEvent.getOrderTotalPrice())
        );

        paymentDetailsRepository.save(paymentDetailsEntity);
    }

}
