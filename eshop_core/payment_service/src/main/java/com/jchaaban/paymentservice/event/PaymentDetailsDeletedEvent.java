package com.jchaaban.paymentservice.event;

import lombok.Data;

@Data
public class PaymentDetailsDeletedEvent {
    private final String paymentDetailsId;
}
