package com.jchaaban.paymentservice;

import com.jchaaban.paymentservice.config.XStreamSecurityConfig;
import com.jchaaban.paymentservice.intercepter.command.CommandInterceptor;
import com.jchaaban.paymentservice.intercepter.query.QueryInterceptor;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.queryhandling.QueryBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

@Import({ XStreamSecurityConfig.class })
@SpringBootApplication
public class PaymentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentServiceApplication.class, args);
    }

    @Autowired
    public void registerInterceptors(
            ApplicationContext context,
            CommandBus commandBus,
            QueryBus queryBus
    ){
        commandBus.registerDispatchInterceptor(context.getBean(CommandInterceptor.class));
        queryBus.registerHandlerInterceptor(context.getBean(QueryInterceptor.class));
    }
}
