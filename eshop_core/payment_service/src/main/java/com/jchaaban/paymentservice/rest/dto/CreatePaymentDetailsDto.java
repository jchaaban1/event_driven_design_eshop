package com.jchaaban.paymentservice.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CreatePaymentDetailsDto {

    @JsonProperty(required = true)
    private final String userId;
    @JsonProperty(required = true)
    private final String cardNumber;
    @JsonProperty(required = true)
    private final int validUntilMonth;
    @JsonProperty(required = true)
    private final int validUntilYear;
    @JsonProperty(required = true)
    private final String cvv;
    @JsonProperty(required = true)
    private final BigDecimal balance;

}
