package com.jchaaban.paymentservice.repository;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import java.math.BigDecimal;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Entity
@Table(name = "payment_details")
public class PaymentDetailsEntity {

    @Id
    private String paymentDetailsId;
    private String userId;
    private String cardNumber;
    private int validUntilMonth;
    private int validUntilYear;
    private String cvv;
    private BigDecimal balance;
}
