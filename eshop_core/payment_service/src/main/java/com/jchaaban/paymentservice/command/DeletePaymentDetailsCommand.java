package com.jchaaban.paymentservice.command;

import lombok.Getter;

@Getter
public class DeletePaymentDetailsCommand extends PaymentDetailsCommand {
    public DeletePaymentDetailsCommand(String paymentDetailsId) {
        super(paymentDetailsId);
    }
}
