package com.jchaaban.paymentservice.rest.exception;

public class DuplicateCardNumberException extends PaymentServiceException {
    public DuplicateCardNumberException(String message) {
        super(message);
    }
}
