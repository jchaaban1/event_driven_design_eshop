package com.jchaaban.paymentservice.rest.exception;

import lombok.Data;

@Data
public class PaymentServiceErrorResponse {
    private final String errorMessage;
}
