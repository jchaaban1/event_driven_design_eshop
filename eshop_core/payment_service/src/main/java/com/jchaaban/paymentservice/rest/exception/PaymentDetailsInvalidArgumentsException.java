package com.jchaaban.paymentservice.rest.exception;

public class PaymentDetailsInvalidArgumentsException extends PaymentServiceException {

    public PaymentDetailsInvalidArgumentsException(String message) {
        super(message);
    }
}
