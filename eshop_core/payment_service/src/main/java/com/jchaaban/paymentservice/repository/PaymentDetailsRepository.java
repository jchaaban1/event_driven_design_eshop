package com.jchaaban.paymentservice.repository;


import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentDetailsRepository extends JpaRepository<PaymentDetailsEntity, String> {
    PaymentDetailsEntity findByPaymentDetailsId(String paymentDetailsId);
    PaymentDetailsEntity findByCardNumber(String cardNumber);
    PaymentDetailsEntity findByUserId(String userId);
}
