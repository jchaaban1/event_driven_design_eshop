package com.jchaaban.userservice.handler.query;

import com.jchaaban.common.model.Address;
import com.jchaaban.common.query.CheckUserExistsQuery;
import com.jchaaban.common.query.FetchUserAddressQuery;
import com.jchaaban.userservice.query.FetchUsersQuery;
import com.jchaaban.userservice.query.query.FetchUserQuery;
import com.jchaaban.userservice.repository.AddressEntity;
import com.jchaaban.userservice.repository.UserEntity;
import com.jchaaban.userservice.repository.UserRepository;
import com.jchaaban.userservice.rest.dto.ReadUserDto;
import com.jchaaban.userservice.rest.exception.UserNotFoundException;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserFetchingQueryHandler {
    private final UserRepository userRepository;

    public UserFetchingQueryHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @QueryHandler
    public List<ReadUserDto> fetchUsers(FetchUsersQuery fetchUsersQuery){
        ArrayList<ReadUserDto> users = new ArrayList<>();
        List<UserEntity> storedProducts = userRepository.findAll();

        storedProducts.forEach(
                userEntity ->
                    users.add(new ReadUserDto(
                        userEntity.getUserId(),
                        userEntity.getFirstname(),
                        userEntity.getLastname(),
                            new Address(
                                    userEntity.getAddress().getStreetAddress(),
                                    userEntity.getAddress().getCity(),
                                    userEntity.getAddress().getState(),
                                    userEntity.getAddress().getPostalCode(),
                                    userEntity.getAddress().getCountry()
                            )
                ))
        );

        return users;
    }

    @QueryHandler
    public ReadUserDto fetchUser(FetchUserQuery fetchUserQuery){
        String userId = fetchUserQuery.getUserId();
        UserEntity userEntity = userRepository.findUserByUserId(userId);

        if (userEntity == null){
            throw new UserNotFoundException("The user with the ID " + userId + " doesn't exist");
        }

        Address address = new Address(
                userEntity.getAddress().getStreetAddress(),
                userEntity.getAddress().getCity(),
                userEntity.getAddress().getState(),
                userEntity.getAddress().getPostalCode(),
                userEntity.getAddress().getCountry()
        );

        return new ReadUserDto(
                userEntity.getUserId(),
                userEntity.getFirstname(),
                userEntity.getLastname(),
                address
        );
    }


    @QueryHandler
    public boolean checkUserExist(CheckUserExistsQuery checkUserExistsQuery){
        String userId = checkUserExistsQuery.getUserId();
        UserEntity userEntity = userRepository.findUserByUserId(userId);

        return userEntity != null;
    }

    @QueryHandler
    public Address fetchUserAddress(FetchUserAddressQuery fetchUserAddressQuery){
        String userId = fetchUserAddressQuery.getUserId();
        UserEntity userEntity = userRepository.findUserByUserId(userId);

        AddressEntity userEntityAddress = userEntity.getAddress();

        return new Address(
                userEntityAddress.getStreetAddress(),
                userEntityAddress.getCity(),
                userEntityAddress.getState(),
                userEntityAddress.getPostalCode(),
                userEntityAddress.getCountry()
        );
    }
}
