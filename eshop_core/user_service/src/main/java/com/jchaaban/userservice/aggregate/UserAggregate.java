package com.jchaaban.userservice.aggregate;

import com.jchaaban.common.model.Address;
import com.jchaaban.userservice.command.CreateUserCommand;
import com.jchaaban.userservice.command.DeleteUserCommand;
import com.jchaaban.userservice.command.UpdateUserCommand;
import com.jchaaban.userservice.event.UserCreatedEvent;
import com.jchaaban.userservice.event.UserDeletedEvent;
import com.jchaaban.userservice.event.UserUpdatedEvent;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aggregate
@NoArgsConstructor
@Component
public class UserAggregate {

    @AggregateIdentifier
    private String userId;
    private String firstname;
    private String lastname;
    private Address address;

    private static final Logger logger = Logger.getLogger(UserAggregate.class.getName());

    @CommandHandler
    public UserAggregate(CreateUserCommand createUserCommand) {
        logger.info("We are in on " + createUserCommand.getClass().getName());
        UserCreatedEvent createUserEvent = new UserCreatedEvent(
                createUserCommand.getUserId(),
                createUserCommand.getFirstname(),
                createUserCommand.getLastname(),
                createUserCommand.getAddress()
        );
        AggregateLifecycle.apply(createUserEvent);
    }

    @EventSourcingHandler
    public void on(UserCreatedEvent userCreatedEvent) {
        logger.info("We are in on " + userCreatedEvent.getClass().getName());
        userId = userCreatedEvent.getUserId();
        firstname = userCreatedEvent.getFirstname();
        lastname = userCreatedEvent.getLastname();
        address = userCreatedEvent.getAddress();
    }

    @CommandHandler
    public void handle(UpdateUserCommand editUserCommand) {
        logger.info("We are in on " + editUserCommand.getClass().getName());
        UserUpdatedEvent editUserEvent = new UserUpdatedEvent(
                editUserCommand.getUserId(),
                editUserCommand.getFirstname(),
                editUserCommand.getLastname(),
                editUserCommand.getAddress()
        );
        AggregateLifecycle.apply(editUserEvent);
    }

    @EventSourcingHandler
    public void on(UserUpdatedEvent userUpdatedEvent) {
        logger.info("We are in on " + userUpdatedEvent.getClass().getName());
        userId = userUpdatedEvent.getUserId();
        firstname = userUpdatedEvent.getFirstname();
        lastname = userUpdatedEvent.getLastname();
        address = userUpdatedEvent.getAddress();
    }

    @CommandHandler
    public void handle(DeleteUserCommand deleteUserCommand) {
        logger.info("We are in on " + deleteUserCommand.getClass().getName());
        UserDeletedEvent userDeletedEvent = new UserDeletedEvent(
                deleteUserCommand.getUserId()
        );
        AggregateLifecycle.apply(userDeletedEvent);
    }

    @EventSourcingHandler
    public void on(UserDeletedEvent userDeletedEvent) {
        logger.info("We are in on " + userDeletedEvent.getClass().getName());
        userId = userDeletedEvent.getUserId();
    }
}
