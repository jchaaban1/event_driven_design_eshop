package com.jchaaban.userservice.rest.controller.command;

import com.jchaaban.userservice.rest.dto.ReadUserDto;
import com.jchaaban.userservice.command.CreateUserCommand;
import com.jchaaban.userservice.command.DeleteUserCommand;
import com.jchaaban.userservice.command.UpdateUserCommand;
import com.jchaaban.userservice.rest.dto.CreateUserDto;
import com.jchaaban.userservice.rest.dto.UpdateUserDto;
import com.jchaaban.userservice.rest.exception.DuplicatedFullNameException;
import com.jchaaban.userservice.rest.exception.UserInvalidArgumentsException;
import com.jchaaban.userservice.rest.exception.UserNotFoundException;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserCommandController {
    private final CommandGateway commandGateway;
    public UserCommandController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PostMapping
    public ResponseEntity<ReadUserDto> createUser(@RequestBody CreateUserDto createUserDto){
        String userId = UUID.randomUUID().toString();
        CreateUserCommand createProductCommand = new CreateUserCommand(
                userId,
                createUserDto.getFirstname(),
                createUserDto.getLastname(),
                createUserDto.getAddress()
        );

        commandGateway.sendAndWait(createProductCommand);

        return new ResponseEntity<>(
                createProductCommand.toToReadProductDto(), HttpStatus.OK
        );
    }

    @PutMapping("/{userId}")
    public ResponseEntity<ReadUserDto> editUser(@PathVariable String userId, @RequestBody UpdateUserDto editUserDto){
        UpdateUserCommand editUserCommand = new UpdateUserCommand(
                userId,
                editUserDto.getFirstname(),
                editUserDto.getLastname(),
                editUserDto.getAddress()
        );

        commandGateway.sendAndWait(editUserCommand);

        return new ResponseEntity<>(
                editUserCommand.toToReadProductDto(), HttpStatus.OK
        );
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable String userId) {

        DeleteUserCommand deleteProductCommand = new DeleteUserCommand(userId);

        commandGateway.sendAndWait(deleteProductCommand);

        return new ResponseEntity<>(
                "User was successfully deleted", HttpStatus.OK
        );
    }

    @ExceptionHandler(UserInvalidArgumentsException.class)
    public ResponseEntity<String> handleUserInvalidArgumentsException(
            UserInvalidArgumentsException productInvalidArgumentsException
    ) {
        return new ResponseEntity<>(productInvalidArgumentsException.getMessage(), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(DuplicatedFullNameException.class)
    public ResponseEntity<String> handleDuplicatedFullNameException(
            DuplicatedFullNameException duplicatedFullNameException
    ) {
        return new ResponseEntity<>(duplicatedFullNameException.getMessage(), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> handleUserNotFoundException(
            UserNotFoundException userNotFoundException
    ) {
        return new ResponseEntity<>(userNotFoundException.getMessage(), HttpStatus.NOT_FOUND);
    }
}
