package com.jchaaban.userservice.rest.exception;
public class UserNotFoundException extends UserServiceException {
    public UserNotFoundException(String productId) {
        super("The user with the id " + productId + " doesn't exist");
    }
}
