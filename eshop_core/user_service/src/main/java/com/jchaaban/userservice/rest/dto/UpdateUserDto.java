package com.jchaaban.userservice.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jchaaban.common.model.Address;
import lombok.Data;

@Data
public class UpdateUserDto {
    @JsonProperty(required = true)
    private final String firstname;
    @JsonProperty(required = true)
    private final String lastname;
    @JsonProperty(required = true)
    private final Address address;
}
