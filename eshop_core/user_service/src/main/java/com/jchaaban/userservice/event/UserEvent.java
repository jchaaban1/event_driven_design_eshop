package com.jchaaban.userservice.event;

import lombok.Data;

@Data
public class UserEvent {
    private final String userId;
}
