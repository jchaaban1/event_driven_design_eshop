package com.jchaaban.userservice.event;

import lombok.Data;

@Data
public class UserDeletedEvent {

    private final String userId;
}