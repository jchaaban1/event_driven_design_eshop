package com.jchaaban.userservice.command;

import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;
@Data
public class UserCommand {
    @TargetAggregateIdentifier
    protected final String userId;
}
