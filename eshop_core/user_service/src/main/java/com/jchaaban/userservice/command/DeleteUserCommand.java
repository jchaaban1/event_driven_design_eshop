package com.jchaaban.userservice.command;

import lombok.Getter;

@Getter
public class DeleteUserCommand extends UserCommand {
    public DeleteUserCommand(String userId) {
        super(userId);
    }
}
