package com.jchaaban.userservice.event;

import com.jchaaban.common.model.Address;
import lombok.Data;

@Data
public class UserCreatedEvent {
    private final String userId;
    private final String firstname;
    private final String lastname;
    private final Address address;
}
