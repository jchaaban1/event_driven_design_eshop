package com.jchaaban.userservice.event;

import com.jchaaban.common.model.Address;
import lombok.Data;

@Data
public class UserUpdatedEvent {
    private final String userId;
    private final String firstname;
    private final String lastname;
    private final Address address;
}
