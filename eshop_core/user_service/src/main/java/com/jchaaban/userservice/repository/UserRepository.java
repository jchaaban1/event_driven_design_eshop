package com.jchaaban.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, String> {
    UserEntity findUserByUserId(String userId);
    UserEntity findUserByFirstnameAndLastname(String firstname, String lastname);
}
