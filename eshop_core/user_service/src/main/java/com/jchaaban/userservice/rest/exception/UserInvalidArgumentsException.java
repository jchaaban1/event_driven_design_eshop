package com.jchaaban.userservice.rest.exception;

public class UserInvalidArgumentsException extends UserServiceException {
    public UserInvalidArgumentsException(String message) {
        super(message);
    }
}
