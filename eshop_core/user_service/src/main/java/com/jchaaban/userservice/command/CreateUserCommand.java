package com.jchaaban.userservice.command;

import com.jchaaban.userservice.rest.dto.ReadUserDto;
import com.jchaaban.common.model.Address;
import lombok.Getter;

@Getter
public class CreateUserCommand extends UserCommand {

    private final String firstname;
    private final String lastname;
    private final Address address;

    public CreateUserCommand(
            String userId,
            String firstname,
            String lastname,
            Address address
    ) {
        super(userId);
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
    }

    public ReadUserDto toToReadProductDto() {
        return new ReadUserDto(
                userId,
                firstname,
                lastname,
                address
        );
    }
}
