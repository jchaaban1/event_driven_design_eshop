package com.jchaaban.userservice.intercepter.query;

import com.jchaaban.userservice.rest.exception.UserNotFoundException;
import org.axonframework.messaging.InterceptorChain;
import org.axonframework.messaging.MessageHandlerInterceptor;
import org.axonframework.messaging.unitofwork.UnitOfWork;
import org.axonframework.queryhandling.QueryExecutionException;
import org.axonframework.queryhandling.QueryMessage;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
public class QueryInterceptor implements MessageHandlerInterceptor<QueryMessage<?, ?>> {
    @Override
    public Object handle(@Nonnull UnitOfWork<? extends QueryMessage<?, ?>> unitOfWork, InterceptorChain interceptorChain) throws Exception {
        try {
            return interceptorChain.proceed();
        } catch (UserNotFoundException exception) {
            throw new QueryExecutionException(
                    exception.getMessage(), exception
            );
        }
    }
}