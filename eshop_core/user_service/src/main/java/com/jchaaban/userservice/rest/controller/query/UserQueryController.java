package com.jchaaban.userservice.rest.controller.query;

import com.jchaaban.userservice.rest.dto.ReadUserDto;
import com.jchaaban.common.query.CheckUserExistsQuery;
import com.jchaaban.userservice.query.FetchUsersQuery;
import com.jchaaban.userservice.rest.exception.UserServiceErrorResponse;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryExecutionException;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserQueryController {

    private final QueryGateway queryGateWay;

    public UserQueryController(QueryGateway queryGateWay) {
        this.queryGateWay = queryGateWay;
    }

    @GetMapping
    public List<ReadUserDto> getUsers() {
        FetchUsersQuery fetchUsersQuery = new FetchUsersQuery();
        return queryGateWay.query(
                fetchUsersQuery,
                ResponseTypes.multipleInstancesOf(ReadUserDto.class)
        ).join();
    }

    @GetMapping("/{userId}")
    public ReadUserDto getUser(@PathVariable String userId) {
        CheckUserExistsQuery fetchUserQuery = new CheckUserExistsQuery(userId);
        return queryGateWay.query(
                fetchUserQuery,
                ResponseTypes.instanceOf(ReadUserDto.class)
        ).join();
    }

    @ExceptionHandler(QueryExecutionException.class)
    public ResponseEntity<UserServiceErrorResponse> handleQueryExecution(
            QueryExecutionException queryExecutionException
    ) {
        String errorMessage = queryExecutionException.getMessage();
        UserServiceErrorResponse errorResponse = new UserServiceErrorResponse(errorMessage);

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}
