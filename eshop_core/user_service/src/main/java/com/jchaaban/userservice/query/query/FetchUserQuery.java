package com.jchaaban.userservice.query.query;

import lombok.Data;

@Data
public class FetchUserQuery {

    private final String userId;
}
