package com.jchaaban.userservice.command.validator;

import com.jchaaban.common.model.Address;
import com.jchaaban.userservice.command.CreateUserCommand;
import com.jchaaban.userservice.command.UpdateUserCommand;
import com.jchaaban.userservice.command.UserCommand;
import com.jchaaban.userservice.repository.UserEntity;
import com.jchaaban.userservice.repository.UserRepository;
import com.jchaaban.userservice.rest.exception.DuplicatedFullNameException;
import com.jchaaban.userservice.rest.exception.UserInvalidArgumentsException;
import com.jchaaban.userservice.rest.exception.UserNotFoundException;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

@Component
public class UserCommandValidator {
    private static final String USER_NOT_FOUND = "User not found";
    private static final String DUPLICATED_FULL_NAME = "This full name is already used by another user";
    private static final String FIRST_NAME_INVALID = "First name is invalid";
    private static final String LAST_NAME_INVALID = "Last name is invalid";
    private static final String STREET_ADDRESS_INVALID = "Street address is invalid";
    private static final String CITY_INVALID = "City is invalid";
    private static final String STATE_INVALID = "State is invalid";
    private static final String COUNTRY_INVALID = "Country is invalid";
    private static final String POSTAL_CODE_INVALID = "Postal code is invalid";
    private static final Predicate<String> NOT_EMPTY = stringField -> stringField != null && !stringField.trim().isEmpty();
    private static final Predicate<String> IS_DIGIT = stringField -> stringField.chars().allMatch(Character::isDigit);
    private static final Predicate<String> VALID_POSTAL_CODE = stringField ->
            (stringField.length() == 5 || stringField.length() == 9) && IS_DIGIT.test(stringField);

    private final UserRepository userRepository;
    public UserCommandValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void validate(UserCommand userCommand) {
        if (userCommand instanceof CreateUserCommand) {
            validateCreateUserCommand((CreateUserCommand) userCommand);
        } else if (userCommand instanceof UpdateUserCommand) {
            validateUpdateUserCommand((UpdateUserCommand) userCommand);
        } else {
            validateUserIdExists(userCommand.getUserId());
        }
    }

    private void validateCreateUserCommand(CreateUserCommand createUserCommand) {
        validateNameFields(createUserCommand.getFirstname(), createUserCommand.getLastname());
        validateAddress(createUserCommand.getAddress());
        validateUniqueFullName(createUserCommand.getFirstname(), createUserCommand.getLastname());
    }

    private void validateUpdateUserCommand(UpdateUserCommand updateUserCommand) {
        validateNameFields(updateUserCommand.getFirstname(), updateUserCommand.getLastname());
        validateAddress(updateUserCommand.getAddress());
        validateUserIdExists(updateUserCommand.getUserId());
        validateUniqueFullName(updateUserCommand.getFirstname(), updateUserCommand.getLastname(), updateUserCommand.getUserId());
    }

    private void validateNameFields(String firstname, String lastname) {
        validateField(NOT_EMPTY, firstname, FIRST_NAME_INVALID);
        validateField(NOT_EMPTY, lastname, LAST_NAME_INVALID);
    }

    private void validateUserIdExists(String userId) {
        if (userRepository.findUserByUserId(userId) == null) {
            throw new UserNotFoundException(USER_NOT_FOUND);
        }
    }

    private void validateUniqueFullName(String firstname, String lastname) {
        if (userRepository.findUserByFirstnameAndLastname(firstname, lastname) != null) {
            throw new DuplicatedFullNameException(DUPLICATED_FULL_NAME);
        }
    }

    private void validateUniqueFullName(String firstname, String lastname, String userId) {
        UserEntity userEntity = userRepository.findUserByFirstnameAndLastname(firstname, lastname);
        if (userEntity != null && !userEntity.getUserId().equals(userId)) {
            throw new DuplicatedFullNameException(DUPLICATED_FULL_NAME);
        }
    }

    private void validateAddress(Address address) {
        validateField(NOT_EMPTY, address.getStreetAddress(), STREET_ADDRESS_INVALID);
        validateField(NOT_EMPTY, address.getCity(), CITY_INVALID);
        validateField(NOT_EMPTY, address.getState(), STATE_INVALID);
        validateField(NOT_EMPTY, address.getCountry(), COUNTRY_INVALID);
        validateField(VALID_POSTAL_CODE, address.getPostalCode(), POSTAL_CODE_INVALID);
    }

    private void validateField(Predicate<String> predicate, String field, String errorMessage) {
        if (!predicate.test(field)) {
            throw new UserInvalidArgumentsException(errorMessage);
        }
    }
}
