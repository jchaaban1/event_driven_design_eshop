package com.example.shippingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ShippingRepository extends JpaRepository<ShippingEntity, String> {
    ShippingEntity findUserByOrderId(String userId);
}
