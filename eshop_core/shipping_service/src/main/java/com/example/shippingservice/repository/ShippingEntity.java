package com.example.shippingservice.repository;

import com.jchaaban.common.model.ShippingStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "shipping")
public class ShippingEntity {

    @Id
    @Column(name = "shipment_id", nullable = false)
    private String shipmentId;

    @Column(name = "order_id", nullable = false)
    private String orderId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "address_id")
    private AddressEntity address;

    @Enumerated(EnumType.STRING)
    @Column(name = "shipping_status", nullable = false)
    private ShippingStatus shippingStatus;

    @Column(name = "tracking_number", nullable = false)
    private String trackingNumber;

    @Column(name = "shipping_cost", nullable = false)
    private BigDecimal shippingCost;


}
