package com.example.shippingservice.aggregate;

import com.jchaaban.common.command.CreateShipmentCommand;
import com.jchaaban.common.event.ShipmentCreatedEvent;
import com.jchaaban.common.model.Address;
import com.jchaaban.common.model.ShippingStatus;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.logging.Logger;


@Aggregate
@NoArgsConstructor
@Component
public class ShippingAggregate {

    @AggregateIdentifier
    private String shipmentId;
    private String orderId;
    private Address address;
    private ShippingStatus shippingStatus;
    private String trackingNumber;
    private BigDecimal shippingCost;

    private static final Logger logger = Logger.getLogger(ShippingAggregate.class.getName());

    @CommandHandler
    public ShippingAggregate(CreateShipmentCommand createShipmentCommand) {
        ShipmentCreatedEvent productCreatedEvent = new ShipmentCreatedEvent(
                createShipmentCommand.getShipmentId(),
                createShipmentCommand.getOrderId(),
                createShipmentCommand.getAddress(),
                createShipmentCommand.getShippingStatus(),
                createShipmentCommand.getTrackingNumber(),
                createShipmentCommand.getShippingCost(),
                createShipmentCommand.getTotalPrice()
        );

        AggregateLifecycle.apply(productCreatedEvent);
    }

    @EventSourcingHandler
    public void on(ShipmentCreatedEvent shipmentCreatedEvent) {
        logger.info("We are in on " + shipmentCreatedEvent.getClass().getName());
        shipmentId = shipmentCreatedEvent.getShipmentId();
        orderId = shipmentCreatedEvent.getOrderId();
        address = shipmentCreatedEvent.getAddress();
        shippingStatus = shipmentCreatedEvent.getShippingStatus();
        trackingNumber = shipmentCreatedEvent.getTrackingNumber();
        shippingCost = shipmentCreatedEvent.getShippingCost();
    }
}
